numpy==1.12.1
olefile==0.44
Pillow==4.1.1
PyQt5==5.8.2
sip==4.19.2
tqdm==4.14.0
